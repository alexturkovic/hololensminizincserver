#MiniZinc Hololens Air Cargo Server

##Installation

Run

    pip3 install --user .
  

## Running the server

Run

    mzn-hololens-server test_output.json test.json

This will open a server on port 5000.
