#!/usr/bin/env python3

import json
import minizinc
import datetime
import copy
import os
import sys
import asyncio
import websockets
import logging

logging.basicConfig(filename='hololens_server.log', format='%(asctime)s %(message)s', level=logging.INFO)

uld = {"ake" : {"lng" : 144, "lat" : 195, "height" : 153, "cuts" : [[150,0,195,50]], "blocks" : []},
       "pmc_md11f_md" : {"lng" : 317, "lat" : 243, "height" : 244,
                         "cuts" : [[175,244,238,164]],
                         "blocks" : [[0,317,0,10,0,10],[0,317,233,243,0,10],[0,10,0,243,0,10],[307,317,0,243,0,10]]
                        },
       "pmc_md11f_md_cad" : {"lng" : 317, "lat" : 243, "height" : 244,
                             "cuts" : [[175,244,238,164]],
                             "blocks" : [[0,317,0,10,0,10],[0,317,233,243,0,10],[0,10,0,243,0,10],[307,317,0,243,0,10]]
                            },
       "pmc_F_ld" : {"lng" : 243, "lat" : 405, "height" : 153, 
                     "cuts" : [[0,50,44,0], [361,0,405,50]],
                     "blocks" : [[0,243,0,44,0,10], [0,243,361,405,0,10], [0,10,0,405,0,10], [233,243,0,405,0,10]]
                    }
      }

def createData(fd):
    piece = []
    tilt_allowed = []
    lat = []
    lng = []
    height = []
    start_lat = []
    start_lng = []
    start_height = []
    
    for item in fd['loadedItems']:
        piece.append({"e":item['piece_unique_name']})
        tilt_allowed.append(item['allowed_rotations']==63)
        lat.append(item['lat'])
        lng.append(item['lng'])
        height.append(item['height'])
        start_lat.append(item['start_lat'])
        start_lng.append(item['start_lng'])
        start_height.append(item['start_height'])
    
    return {"PIECE" : piece, "lat" : lat, "lng" : lng, "height" : height,
            "start_lat" : start_lat, "start_lng" : start_lng, "start_height" : start_height,
            "tilt_allowed" : tilt_allowed,
            "uld_lat" : uld[fd["ULD_type"]]["lat"],
            "uld_lng" : uld[fd["ULD_type"]]["lng"],
            "uld_height" : uld[fd["ULD_type"]]["height"],
            "uld_cuts" : uld[fd["ULD_type"]]["cuts"],
#            "uld_blocks" : [], #uld[fd["ULD_type"]]["blocks"], # disabled for now
           }

def solve(d, timeLimit=20):
    m = minizinc.Model(os.path.join(os.path.abspath(os.path.dirname(__file__)),"solve_uld.mzn"))
    solver = minizinc.Solver.lookup("cplex")
    if '--cplex-dll' in solver.requiredFlags:
        solver = minizinc.Solver.lookup("cbc")
        logging.info("solve with cbc")
    else:
        logging.info("solve with cplex")
    instance = minizinc.Instance(solver, m)
    for l in d:
        instance[l]=d[l]
    return instance.solve_async(timeout=datetime.timedelta(seconds=timeLimit))

def data2dict(data):
    d = dict()
    d["Staging Area"] = { "pieces" : {}, "uld" : None}
    for uld in data:
        pieces = dict()
        for p in uld["loadedItems"]:
            pieces[p["piece_unique_name"]] = p
        d[uld["ULD_unique_name"]] = { "pieces" : pieces, "uld" : uld }
    return d

def solution2uld(uld,solution):
    loadedItems = []
    for idx, piece in enumerate(uld["loadedItems"]):
        p = copy.deepcopy(piece)
        p["height"] = solution["rotated_height"][idx]
        p["lat"] = solution["rotated_lat"][idx]
        p["lng"] = solution["rotated_lng"][idx]
        p["start_height"] = solution["target_height"][idx]
        p["start_lat"] = solution["target_lat"][idx]
        p["start_lng"] = solution["target_lng"][idx]
        loadedItems.append(p)
    new_uld = copy.copy(uld)
    new_uld["loadedItems"] = loadedItems
    pieces = dict()
    for p in loadedItems:
        pieces[p["piece_unique_name"]] = p
    return new_uld, pieces

async def serve(websocket, path):
    my_flight_data = None
    while True:
        command_str = await websocket.recv()
        if command_str.startswith("LOG"):
            logging.info("%s log message %s", websocket.local_address, command_str)
        elif command_str=="init":
            logging.info("%s initialised", websocket.local_address)
            my_flight_data = copy.deepcopy(flight_data)
            my_flight_data_dict = data2dict(my_flight_data)
            await websocket.send(json.dumps(my_flight_data))
        else:
            try:
                command = json.loads(command_str)
            except:
                logging.info("%s error parsing command", websocket.local_address)
            else:
                if "timeLimit" in command:
                    timeLimit=int(command["timeLimit"])
                else:
                    timeLimit=10
                if "action" in command:
                    if command["action"]=="move":
                        piece = command["piece_unique_name"]
                        from_uld = command["from"]["ULD_unique_name"]
                        to_uld = command["to"]["ULD_unique_name"]
                        logging.info("%s move %s from %s to %s", websocket.local_address, piece, from_uld, to_uld)
                        if from_uld == "Staging Area" and to_uld != "Staging Area":
                            # move from Staging Area into ULD, recompute target
                            d = copy.deepcopy(my_flight_data_dict[to_uld]["uld"])
                            d["loadedItems"].append(my_flight_data_dict["Staging Area"]["pieces"][piece])
                            dzn = createData(d)
                            result = await solve(dzn, timeLimit)
                            if result.status.has_solution():
                                new_uld, new_pieces = solution2uld(d,result)
                                my_flight_data_dict["Staging Area"]["pieces"].pop(piece)
                                my_flight_data_dict[to_uld]["uld"]=new_uld
                                my_flight_data_dict[to_uld]["pieces"]=new_pieces
                                print("new flight data",json.dumps(my_flight_data_dict,indent=2))
                                logging.info("%s move sucessful", websocket.local_address)
                                await websocket.send(json.dumps({"response":"success", "piece_unique_name":piece, "ULD_unique_name": from_uld, "ulds":[new_uld]}))
                            else:
                                logging.info("%s move failed", websocket.local_address)
                                await websocket.send(json.dumps({"response":"fail", "piece_unique_name":piece, "ULD_unique_name": from_uld}))
                        elif from_uld != "Staging Area" and to_uld == "Staging Area":
                            # move from ULD to Staging Area, recompute source
                            d = copy.deepcopy(my_flight_data_dict[from_uld]["uld"])
                            d["loadedItems"] = [i for i in d["loadedItems"] if i["piece_unique_name"] != piece]
                            dzn = createData(d)
                            result = await solve(dzn, timeLimit)
                            if result.status.has_solution():
                                new_uld, new_pieces = solution2uld(d,result)
                                my_flight_data_dict["Staging Area"]["pieces"][piece] = my_flight_data_dict[from_uld]["pieces"][piece]
                                my_flight_data_dict[from_uld]["uld"]=new_uld
                                my_flight_data_dict[from_uld]["pieces"]=new_pieces
                                print("new flight data",json.dumps(my_flight_data_dict,indent=2))
                                logging.info("%s move sucessful", websocket.local_address)
                                await websocket.send(json.dumps({"response":"success", "piece_unique_name":piece,"ULD_unique_name": from_uld, "ulds":[new_uld]}))
                            else:
                                logging.info("%s move failed", websocket.local_address)
                                await websocket.send(json.dumps({"response":"fail", "piece_unique_name":piece, "ULD_unique_name": from_uld}))
                        else:
                            # move from ULD to ULD, recompute both
                            d_from = copy.deepcopy(my_flight_data_dict[from_uld]["uld"])
                            d_from["loadedItems"] = [i for i in d_from["loadedItems"] if i["piece_unique_name"] != piece]
                            dzn = createData(d_from)
                            from_result = await solve(dzn, timeLimit)
                            if from_result.status.has_solution():
                                d_to = copy.deepcopy(my_flight_data_dict[to_uld]["uld"])
                                new_piece = copy.deepcopy(my_flight_data_dict[from_uld]["pieces"][piece])
                                new_piece["start_lat"] = -1 # this means that it's recognised as a new piece and can be inserted anywhere
                                d_to["loadedItems"].append(new_piece)
                                dzn = createData(d_to)
                                to_result = await solve(dzn, timeLimit)
                                if to_result.status.has_solution():
                                    new_to_uld, new_to_pieces = solution2uld(d_to,to_result)
                                    my_flight_data_dict[to_uld]["uld"]=new_to_uld
                                    my_flight_data_dict[to_uld]["pieces"]=new_to_pieces
                                    new_from_uld, new_from_pieces = solution2uld(d_from,from_result)
                                    my_flight_data_dict[from_uld]["uld"]=new_from_uld
                                    my_flight_data_dict[from_uld]["pieces"]=new_from_pieces
                                    print("new flight data",json.dumps(my_flight_data_dict,indent=2))
                                    logging.info("%s move sucessful", websocket.local_address)
                                    await websocket.send(json.dumps({"response":"success", "piece_unique_name":piece, "ULD_unique_name": from_uld, "ulds":[new_from_uld,new_to_uld]}))
                                else:
                                    logging.info("%s move failed", websocket.local_address)
                                    await websocket.send(json.dumps({"response":"fail", "piece_unique_name":piece, "ULD_unique_name": from_uld}))
                            else:
                                logging.info("%s move failed", websocket.local_address)
                                await websocket.send(json.dumps({"response":"fail", "piece_unique_name":piece, "ULD_unique_name": from_uld}))
                    else:
                        logging.info("%s invalid action %s", websocket.local_address, command["action"])
                        print("Cannot handle action "+str(command))
                else:
                    logging.info("%s invalid command, no action", websocket.local_address)
                    print("Cannot handle command "+command)

def run(fdata_filename):
    global flight_data
    with open(fdata_filename, "r") as f:
        flight_data = json.load(f)

    start_server = websockets.serve(serve, "0.0.0.0", 5000)

    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    run(sys.argv[1])
