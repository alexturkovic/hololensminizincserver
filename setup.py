#!/usr/bin/env python3
import setuptools

setuptools.setup(
    name="mzn_hololens_server", # Replace with your own username
    version="0.0.1",
    author="Guido Tack",
    author_email="guido.tack@monash.edu",
    packages=setuptools.find_packages(),
    install_requires=[
              'minizinc',
              'websockets'
    ],
    include_package_data=True,
    scripts = ['bin/mzn-hololens-server'],
    python_requires='>=3.6',
)
